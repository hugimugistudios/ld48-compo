/// @desc Variables

//package
package = noone;

//maximum distance
maxdist_def = sprite_width*2;
maxdist = maxdist_def;

//mp path
mp_path = path_add();

//destination
destination = [obj_package_destinationpoint.x,obj_package_destinationpoint.y];