/// @desc Move to destination point

//check if package is close
if distance_to_object(package) < maxdist
{	
	//check if not stuck
	if mp_grid_path(global.mp_grid,mp_path,x,y,destination[0],destination[1],true)
	{
		//start path
		path_start(mp_path,maxdist/3,path_action_stop,false);
	}
}
else
{
	//end path
	path_end();
	
	//teleport to package
	x = package.x;
	y = package.y;
}

//calculate max distance depending on how fast package is moving
maxdist = (package.follow_speed)+maxdist_def;