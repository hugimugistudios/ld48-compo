/// @desc Room start init

#region Replace placeholder tiles

//variables
var temp_layer = layer_get_id("Temptiles");
var tile_layer = layer_get_id("Tiles");
var dkooze_tmap = layer_tilemap_get_id(temp_layer);
var cell_size = 32;
var xcell_amount = floor(room_width/cell_size);
var ycell_amount = floor(room_height/cell_size);
var cell_amount = xcell_amount*ycell_amount;

//hide the temp layer
layer_set_visible(temp_layer,false);

//check every tile on the tilemap
for(var i = 0, X = 0, Y = 0; i <= cell_amount; i++)
{
	//get y
	Y = floor(i/xcell_amount);

	//get x
	X = (i-(Y*xcell_amount));

	//check if there is tile
	var tle = tilemap_get(dkooze_tmap,X,Y);
	if tle > 0
	{
		//check which tile and create it
		switch(tle-1)
		{
			case mineral.rock:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_rock);
				break;
			
			case mineral.bauxite:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_bauxite);
				break;
				
			case mineral.phosphorus:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_phosphorus);
				break;
				
			case mineral.iron:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_iron);
				break;
				
			case mineral.obsidian:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_obsidian);
				break;
				
			case mineral.gold:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_gold);
				break;
				
			case mineral.diamond:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_diamond);
				break;
				
			case mineral.volcanium:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_volcanium);
				break;
				
			case mineral.aluminium:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_aluminium);
				break;
				
			case mineral.carbonite:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_carbonite);
				break;
				
			case mineral.daurite:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_daurite);
				break;
				
			case mineral.zirconium:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_zirconium);
				break;
				
			case mineral.diamonite:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_diamonite);
				break;
				
			case mineral.ludium:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_ludium);
				break;
				
			case mineral.ludumdaurite48:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_ludumdaurite48);
				break;
				
			case mineral.hyperdiamonite:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile_hyperdiamonite);
				break;
			
			default:
				instance_create_layer(X*cell_size,Y*cell_size,tile_layer,obj_tile);
				break;
		}
	}
}

#endregion

#region Update tiles

//update tiles
with(obj_tile)
{
	//variables
	var neighbs = [], tilesz = sprite_width;
	
	//centerpoint
	var cx = x+(tilesz/2), cy = y+(tilesz/2);
	
	//get neighbouring tiles
	neighbs[0] = collision_point(cx+tilesz,cy,obj_tile,false,true);	//right
	neighbs[1] = collision_point(cx+tilesz,cy-tilesz,obj_tile,false,true);	//rightup
	neighbs[2] = collision_point(cx,cy-tilesz,obj_tile,false,true);	//up
	neighbs[3] = collision_point(cx-tilesz,cy-tilesz,obj_tile,false,true);	//leftup
	neighbs[4] = collision_point(cx-tilesz,cy,obj_tile,false,true);	//left
	neighbs[5] = collision_point(cx-tilesz,cy+tilesz,obj_tile,false,true);	//leftdown
	neighbs[6] = collision_point(cx,cy+tilesz,obj_tile,false,true);	//down
	neighbs[7] = collision_point(cx+tilesz,cy+tilesz,obj_tile,false,true);	//rightdown
	
	#region Change appearance accordingly
	
	//topleft open
	if neighbs[0] && neighbs[2] == noone && neighbs[4] == noone && neighbs[6]
	{
		image_index = 0;
	}
	
	//top
	if neighbs[0] && neighbs[2] == noone && neighbs[4] && neighbs[6]
	{
		image_index = 1;
	}
	
	//topright open
	if neighbs[0] == noone && neighbs[2] == noone && neighbs[4] && neighbs[6]
	{
		image_index = 2;
	}
	
	//left
	if neighbs[0] && neighbs[2] && neighbs[4] == noone && neighbs[6]
	{
		image_index = 3;
	}
	
	//middle
	if neighbs[0] && neighbs[2] && neighbs[4] && neighbs[6]
	{
		image_index = 4;
	}
	
	//right
	if neighbs[0] == noone && neighbs[2] && neighbs[4] && neighbs[6]
	{
		image_index = 5;
	}
	
	//bottomleft open
	if neighbs[0] && neighbs[2] && neighbs[4] == noone && neighbs[6] == noone
	{
		image_index = 6;
	}
	
	//bottom
	if neighbs[0] && neighbs[2] && neighbs[4] && neighbs[6] == noone
	{
		image_index = 7;
	}
	
	//bottomright open
	if neighbs[0] == noone && neighbs[2] && neighbs[4] && neighbs[6] == noone
	{
		image_index = 8;
	}
	
	//topleft closed
	if neighbs[0] && neighbs[2] && neighbs[4] && neighbs[6] && neighbs[7] == noone
	{
		image_index = 9;
	}
	
	//topright closed
	if neighbs[0] && neighbs[2] && neighbs[4] && neighbs[5] == noone && neighbs[6]
	{
		image_index = 10;
	}
	
	//bottomleft closed
	if neighbs[0] && neighbs[1] == noone && neighbs[2] && neighbs[4] && neighbs[6]
	{
		image_index = 11;
	}
	
	//bottomright closed
	if neighbs[0] && neighbs[2] && neighbs[3] == noone && neighbs[4] && neighbs[6]
	{
		image_index = 12;
	}
	
	#endregion
}

#endregion

#region First mission

//check if correct room
if room == rm_1
{
	//check if already timer
	if alarm[0] == -1
	{
		//start mission timer
		alarm[0] = ms_timer;
	}
}

#endregion

#region Deactivation timer

//check room
if room == rm_1
{
	//check if already timer
	if alarm[1] == -1
	{
		//start timer
		alarm[1] = de_timer;
	}
}

#endregion