/// @desc Mission timer

//check if correct room
if room == rm_1
{
	//check if mission completed or first mission
	if mis_completed xor mission[? "index"] == -1
	{
		//reset complete
		mis_completed = false;
		
		//check if more missions
		if !is_undefined(variable_struct_get(global.missions,mission[? "index"]+1))
		{
			//start mission
			mission_start(mission[? "index"]+1);
		}
		else
		{
			//last mission completed
			missions_end = true;
		}
	}
}

//stop timer
alarm[0] = -1;