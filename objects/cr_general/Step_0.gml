/// @desc General

#region cursor

//check if cursor exists
if !instance_exists(cursor)
{
	//create
	instance_create_layer(mouse_x,mouse_y,"Controllers",cursor);
}

#endregion

#region game controls

//exit game
if keyboard_check_pressed(vk_escape)
{
	game_end();
}

//restart games
if keyboard_check_pressed(vk_f5)
{
	game_restart();
}

//toggle fullscreen
if keyboard_check_pressed(vk_f11)
{
	window_set_fullscreen(!window_get_fullscreen());
}

//toggle fps counter
if keyboard_check_pressed(vk_f1)
{
	cr_graphics.fps_counter = 1-cr_graphics.fps_counter;
}

#endregion

#region Missions

//check if completed mission
if mis_completed
{
	//check if already timer
	if alarm[0] == -1
	{
		//start mission timer for next mission
		alarm[0] = ms_timer;
	}
}

#endregion