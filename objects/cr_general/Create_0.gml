/// @desc Variables

//mission completed
mis_completed = false;

//active mission map
mission = ds_map_create();

//set mission keys
mission[? "index"] = -1;
mission[? "name"] = ""
mission[? "objective"] = [];
mission[? "log"] = [];
mission[? "mlog"] = "";
mission[? "log2"] = [];
mission[? "received"] = false;

//mission timer
ms_timer = 1*room_speed;

//end
missions_end = false;

//mp grid updation
mp_update = true;

//object deactivation timer
de_timer = 10;