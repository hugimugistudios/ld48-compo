/// @desc Object deactivation

//activate all objects
instance_activate_all();

#region Update MP grid

//check if need to update
if mp_update
{
	//update MP grid
	var tilesz = 32;
	mp_grid_destroy(global.mp_grid);
	global.mp_grid = mp_grid_create(0,0,room_width/tilesz,room_height/tilesz,32,32);
	mp_grid_add_instances(global.mp_grid,obj_tile,0);
}

#endregion

//variables
var padd_buffer = 128;
var cx = camera_get_view_x(view_camera[0]), cy = camera_get_view_y(view_camera[0]);
var ch = camera_get_view_height(view_camera[0]), cw = camera_get_view_width(view_camera[0]);

//instance activation list
var act_instances = [];

//add everything except excluded objects to activation array for later activation
var exc_object = [obj_tile,obj_mineral];
for(var i = 0; i < instance_number(all); i++)
{
	//get variables
	var ins = instance_find(all,i);
	var ins_ob = ins.object_index;
	var i_parent = object_get_parent(ins_ob);
	var exc = false;
	
	//check if to be excluded from activation
	for(var e = 0; e < array_length(exc_object); e++)
	{
		//check
		if i_parent == exc_object[e]
		{
			//exclude
			exc = true;
			break;
		}
	}
	
	//check if not excluded
	if !exc
	{
		//add to activation array
		act_instances[array_length(act_instances)] = instance_find(all,i);
	}
}

//deactivate all objects outside camera view
instance_deactivate_region(cx-padd_buffer,cy-padd_buffer,cw+padd_buffer*2,ch+padd_buffer*2,false,true);

//activate packages
instance_activate_object(obj_package);

//check if packages exist
if instance_exists(obj_package)
{
	//activate their view regions
	with(obj_package)
	{
		//activate
		var act_sz = 128;
		instance_activate_region(x-act_sz-padd_buffer,y-act_sz-padd_buffer,x+act_sz+padd_buffer*2,y+act_sz+padd_buffer*2,true);
	}
}

//activate everything except excluded objects
for(var i = 0; i < array_length(act_instances); i++)
{
	//activate instance
	instance_activate_object(act_instances[i]);
}

//reset timer
alarm[1] = de_timer;