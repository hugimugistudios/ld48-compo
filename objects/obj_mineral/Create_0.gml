/// @desc Variables and init

//marked to ignore transmutations
ignore_transmutations = false;

//no animation
image_speed = 0;
image_index = 1;

//play sound
var snd = choose(sn_drop1,sn_drop2,sn_drop3);
audio_stop_sound(snd);
audio_play_sound(snd,1,false);