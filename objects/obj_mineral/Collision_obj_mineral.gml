/// @desc Transmutate

//check if ignoring transmutations
if !ignore_transmutations
{
	//variables
	var col_mineral = other.Mineral;
	var midx = (x+other.x)/2, midy = (y+other.y)/2;
	
	//possible transmutation
	var transm = transmutate(Mineral,col_mineral);
	if transm != -1
	{
		//mark reactants to ignore transmutations
		ignore_transmutations = true;
		other.ignore_transmutations = true;
		
		//destroy reactants
		instance_destroy(other);
		instance_destroy();
		
		//transmutation
		mineral_create(midx,midy,transm);
		
		//play sound
		if !audio_is_playing(sn_transm1) && !audio_is_playing(sn_transm2) && !audio_is_playing(sn_transm3)
		{
			var snd = choose(sn_transm1,sn_transm2,sn_transm3);
			audio_stop_sound(snd);
			audio_play_sound(snd,1,false);
		}
	}
}