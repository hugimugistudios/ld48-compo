/// @desc Excavated

//create excavated mineral
mineral_create(x+sprite_width/2,y+sprite_height/2,Mineral);

#region Update neighbours

//variables
var neighbours = [], tilesz = sprite_width;

//centerpoint
var cx = x+(tilesz/2), cy = y+(tilesz/2);

//get neighbouring tiles
neighbours[0] = collision_point(cx+tilesz,cy,obj_tile,false,true);	//right
neighbours[1] = collision_point(cx+tilesz,cy-tilesz,obj_tile,false,true);	//rightup
neighbours[2] = collision_point(cx,cy-tilesz,obj_tile,false,true);	//up
neighbours[3] = collision_point(cx-tilesz,cy-tilesz,obj_tile,false,true);	//leftup
neighbours[4] = collision_point(cx-tilesz,cy,obj_tile,false,true);	//left
neighbours[5] = collision_point(cx-tilesz,cy+tilesz,obj_tile,false,true);	//leftdown
neighbours[6] = collision_point(cx,cy+tilesz,obj_tile,false,true);	//down
neighbours[7] = collision_point(cx+tilesz,cy+tilesz,obj_tile,false,true);	//rightdown

//update neighbouring tiles
for(var n = 0; n < array_length(neighbours); n++)
{
	//check if neighbour exists
	if !instance_exists(neighbours[n])
	{
		//doesn't exist
		continue;
	}
	
	//neighbours update tiles
	with(neighbours[n])
	{
		//set alarm to update
		alarm[0] = 1;
	}
}

#endregion

//update MP grid
cr_general.mp_update = true;