/// @desc Draw self and excavation laser

#region Draw excavation/collection laser

//check if excavating
with(cursor)
{
	//tile
	var tile = collision_point(x,y,obj_tile,false,true);
	if instance_exists(tile) && place_meeting(x,y,tile)
	{
		with(obj_player)
		{
			//get input
			if mouse_check_button(mb_left) && distance_to_object(tile) < exc_radius
			{
				//draw
				draw_set_color(make_color_rgb(exc_color[0],exc_color[1],exc_color[2]));
				draw_line(x,y,tile.x+tile.sprite_width/2,tile.y+tile.sprite_width/2);
				draw_set_color(c_white);
			}
		}
	}
}

//check if collecting
with(cursor)
{
	//mineral
	var mnrl = collision_point(x,y,obj_mineral,false,true);
	if instance_exists(mnrl) && place_meeting(x,y,mnrl)
	{
		with(obj_player)
		{
			//get input
			if mouse_check_button(mb_right) && distance_to_object(mnrl) < exc_radius
			{
				//check inventory
				if inventory[0] == -1 || inventory[0] = mnrl.Mineral
				{
					//draw
					draw_set_color(make_color_rgb(col_color[0],col_color[1],col_color[2]));
					draw_line(x,y,mnrl.x,mnrl.y);
					draw_set_color(c_white);
				}
			}
		}
	}
}

#endregion

//draw self
draw_self();