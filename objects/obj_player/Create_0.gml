/// @desc Variables and init

#region Properties

//movement
horizontal_acceleration = 3;
horizontal_mvelocity = 200;

//buoyancy
buoyancy = 0;
b_step = 1/50;
buoyancy_acceleration = 1;
buoyancy_mvelocity = horizontal_mvelocity/2;

//excavation
excavation_power = 1;
exc_radius = 128;
exc_color = [255,255,255];
col_color = [255,255,255];

#endregion

//inventory
inventory = [-1,0];