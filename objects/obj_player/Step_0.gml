/// @desc Movement

#region Movement

//variables
var h_mov_dir = 0, h_velocity = horizontal_acceleration*h_mov_dir;

//horizontal
var k_left = ord("A"), k_right = ord("D");
if keyboard_check(k_left)
{
	//left
	h_mov_dir += -1;
}
if keyboard_check(k_right)
{
	//right
	h_mov_dir += 1;
}

//animation
if h_mov_dir == 0
{
	//stop animation
	image_speed = 0;
	image_index = 0;
}
else
{
	//continue animation
	image_speed = 1;
}

//doesn't rotate
phy_rotation = 0;

#region Buoyancy

//update buoyancy from valve
buoyancy = obj_hud_valve.value;

//clamp buoyancy
buoyancy = clamp(buoyancy,-1,1);

//normalize buoyancy relative to world gravty
var w_grav = 4;
physics_apply_force(phy_com_x,phy_com_y,0,phy_mass*-w_grav);

#endregion

//apply movement
h_velocity = horizontal_acceleration*h_mov_dir;
phy_linear_velocity_x += h_velocity;
phy_linear_velocity_y += buoyancy_acceleration*buoyancy*-1;

//keep velocity in certain range
phy_linear_velocity_x = clamp(phy_linear_velocity_x,-horizontal_mvelocity,horizontal_mvelocity);
phy_linear_velocity_y = clamp(phy_linear_velocity_y,-buoyancy_mvelocity,buoyancy_mvelocity);

#endregion

#region Animation

//check velocity
if phy_linear_velocity_x != 0
{
	//set animation direction
	if image_xscale != sign(phy_linear_velocity_x)
	{
		image_xscale = sign(phy_linear_velocity_x);
	}
}

#endregion

#region Excavation

//check if cursor is colliding with a tile
with(cursor)
{
	//tile
	var tile = collision_point(x,y,obj_tile,false,true);
	if instance_exists(tile) && place_meeting(x,y,tile)
	{
		with(obj_player)
		{
			//get input
			if mouse_check_button(mb_left) && distance_to_object(tile) < exc_radius
			{
				//mine
				tile.excavation += excavation_power/tile.hardness;
				
				//play sound
				var snd = sn_excavation
				if !audio_is_playing(snd)
				{
					audio_play_sound(snd,1,true);
				}
				
				#region Update tile excavation
				
				//with excavated tile
				with(tile)
				{
					//update apperance
					image_blend = make_color_rgb(clamp((excavation*-1*255)+obj_player.exc_color[0],0,255),clamp((excavation*-1*255)+obj_player.exc_color[1],0,255),clamp((excavation*-1*255)+obj_player.exc_color[2],0,255));
					
					//check if excavated
					if excavation >= 1
					{
						//destroy
						instance_destroy();
					}
				}
				
				#endregion
			}
			else if audio_is_playing(sn_excavation)
			{
				//stop sound
				audio_stop_sound(sn_excavation);
			}
		}
	}
	else if audio_is_playing(sn_excavation)
	{
		//stop sound
		audio_stop_sound(sn_excavation);
	}
}

#endregion

#region Collection

//check if cursor is colliding with a mineral
with(cursor)
{
	//mineral
	var mnrl = collision_point(x,y,obj_mineral,false,true);
	if instance_exists(mnrl) && place_meeting(x,y,mnrl)
	{
		with(obj_player)
		{
			//get input
			if mouse_check_button_released(mb_right) && distance_to_object(mnrl) < exc_radius
			{
				//check inventory
				if inventory[0] == -1 || inventory[0] = mnrl.Mineral
				{
					if inventory[0] == -1 { inventory[0] = mnrl.Mineral; }
					//collect
					inventory[1]++;
					
					//destroy mineral
					instance_destroy(mnrl);
					
					//play sound
					var snd = choose(sn_collect1,sn_collect2,sn_collect3);
					audio_stop_sound(snd);
					audio_play_sound(snd,1,false);
				}
			}
		}
	}
}

#endregion

#region Inventory buttons

//variables
var cx = camera_get_view_x(view_camera[0]), cy = camera_get_view_y(view_camera[0]);
var ch = camera_get_view_height(view_camera[0]), cw = camera_get_view_width(view_camera[0]);
var xpos = 0, ypos = 0, padd = 48;

//distance
var btn_dist = 256;

//check if cursor is close enough and clicked
if instance_exists(cursor) && point_distance(cursor.x,cursor.y,((cx+cw*xpos)+padd),((cy+ch*ypos)+padd)) < btn_dist && mouse_check_button_released(mb_left)
{
	//button hitboxes
	var btn_yoffset = 32, btn_pad = 2, btn1_str = "Drop", btn2_str = "Send";
	var btn1_bx = ((cx+cw*xpos)+padd)+padd*0, btn1_by = (cy+ch*ypos)+padd;
	var btn2_bx = ((cx+cw*xpos)+padd)+padd*1.5, btn2_by = ((cy+ch*ypos)+padd);
	var btn1_colx1 = (btn1_bx-btn_pad);
	var btn1_coly1 = ((btn1_by-btn_pad)-(string_height(btn1_str)/3))+btn_yoffset;
	var btn1_colx2 = btn1_bx+string_width(btn1_str)+btn_pad;
	var btn1_coly2 = (btn1_by+(string_height(btn1_str)/3)+btn_pad*2)+btn_yoffset;
	var btn2_colx1 = (btn2_bx-btn_pad);
	var btn2_coly1 = ((btn2_by-btn_pad)-(string_height(btn2_str)/3))+btn_yoffset;
	var btn2_colx2 = btn2_bx+string_width(btn2_str)+btn_pad;
	var btn2_coly2 = (btn2_by+(string_height(btn2_str)/3)+btn_pad*2)+btn_yoffset;
	
	//check if collding with buttons
	if cursor.x > btn1_colx1 && cursor.x < btn1_colx2 && cursor.y > btn1_coly1 && cursor.y < btn1_coly2
	{
		#region Drop
		
		//create all minerals from inventory
		for(var m = 0; m < inventory[1]; m++)
		{
			//create
			with(mineral_create(x,y+sprite_height,inventory[0]))
			{
				//add randomization to coordinates
				var ran_r = 10;
				phy_position_x += random_range(-ran_r,ran_r);
				phy_position_y += random_range(-ran_r,ran_r);
			}
		}
		
		//remove minerals from inventory
		inventory = [-1,0];
		
		#endregion
	}
	if cursor.x > btn2_colx1 && cursor.x < btn2_colx2 && cursor.y > btn2_coly1 && cursor.y < btn2_coly2
	{
		#region Send
		
		//create package with correct size
		if inventory[1] < 4
		{
			//small
			with(instance_create_layer(x,y-sprite_height,"Instances",obj_package))
			{
				//add contents
				contents = other.inventory;
			}
		}
		else if inventory[1] >= 4 && inventory[1] < 20
		{
			//medium
			with(instance_create_layer(x,y-sprite_height,"Instances",obj_package_medium))
			{
				//add contents
				contents = other.inventory;
			}
		}
		else if inventory[1] >= 20
		{
			//large
			with(instance_create_layer(x,y-sprite_height,"Instances",obj_package_large))
			{
				//add contents
				contents = other.inventory;
			}
		}
		
		//remove minerals from inventory
		inventory = [-1,0];
		
		#endregion
	}
}

#endregion