/// @desc Variables and init

//no animation
image_speed = 0;
image_index = 1;

#region Variables

//is being rotatetd
isrotated = false;

//max rotation
mrotation = 1*360;

//value
value = 0;

#endregion

#region Joints

//create joint
joint = physics_joint_revolute_create(id,instance_nearest(x,y,obj_point),x,y,0,0,false,0,0,false,true);
cursor_joint = -1;

#endregion