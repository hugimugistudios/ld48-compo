/// @desc Draw self

//variables
var cx = camera_get_view_x(view_camera[0]), cy = camera_get_view_y(view_camera[0]);
var ch = display_get_gui_height(), cw = display_get_gui_width();
var xpos = 1, ypos = 0, padd = 128;

//draw
draw_sprite_ext(sprite_index,0,(cx+cw*xpos)-padd,(cy+ch*ypos)+padd/2,1,1,-phy_rotation,c_white,1);