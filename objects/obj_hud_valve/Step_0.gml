/// @desc Revolution and logic

#region Revolution

//release click
if mouse_check_button_released(mb_left)
{
	//delete previous cursor joint
	physics_joint_delete(cursor_joint);
	isrotated = false;
}

//check if cursor is colliding
if place_meeting(x,y,cursor)
{
	//click
	if mouse_check_button_pressed(mb_left)
	{
		//delete previous cursor joint
		physics_joint_delete(cursor_joint);
		
		//create joint
		cursor_joint = physics_joint_revolute_create(id,instance_nearest(x,y,cursor),cursor.x,cursor.y,0,360,false,0,0,false,true);
		isrotated = true;
	}
}
else
{
	//check if far enough
	if distance_to_object(cursor) > sprite_width
	{
		//delete previous cursor joint if active
		if mouse_check_button(mb_left)
		{
			//delete
			physics_joint_delete(cursor_joint);
			isrotated = false;
		}
	}
}

#region Rotations

//check if is not being rotated
if !isrotated
{
	//slowly go back to neutral
	phy_rotation = lerp(phy_rotation,0,obj_player.b_step);
	if (phy_rotation < 1/10 && phy_rotation > 0) xor (phy_rotation > -1/10 && phy_rotation < 0)
	{
		phy_rotation = 0;
	}	
}

//update value of valve
value = phy_rotation/mrotation;

#endregion

#endregion