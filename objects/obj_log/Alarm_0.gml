/// @desc Type character

//variables
var type_letter = string_char_at(text,type_pos);

//increment type position
type_pos++;

//add character to typed text
typed_text += type_letter;

//play sound
var snd = choose(sn_types1,sn_types2,sn_types3,sn_types4);
audio_stop_sound(snd);
audio_play_sound(snd,1,false);

//check if last character
if type_pos > string_length(text)
{
	//stop timer
	alarm[0] = -1; 
}
else
{
	//reset timer
	alarm[0] = type_duration;
}