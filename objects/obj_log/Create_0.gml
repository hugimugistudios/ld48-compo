/// @desc Variables and init

//stop animation
image_speed = 0;
image_index = 0;

#region Variables

//transparency alpha
transparent_alpha = 0.4;

//log text
pending_texts = []
text = "";
typed_text = "";
type_pos = 1;
type_duration = 4;
wordwrap_cutoff = 0.7;

//if ready to display next line
nextready = false;

//fade
text_alpha = 1;
fade_text = false;
fade_step = 1/10;

//continuation text
cont_text = "";
continuation_text = "\n(...)";

#endregion