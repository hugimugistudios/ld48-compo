/// @desc Typing logic

#region Typing

//check if pending texts
if array_length(pending_texts) > 0
{
	//check if ready
	if nextready
	{
		//prepeare for typing
		nextready = false;
		typed_text = "";
		text = pending_texts[0];
		pending_texts[0] = "";
		type_pos = 1;
		alarm[0] = type_duration;
		
		//insert newlines when necessary
		for(var i = 1, s = "", n = ""; i < string_length(text)+1; i++)
		{
			//check if forced newline
			if string_char_at(text,i) == "#"
			{
				//add newline to new text
				n += "\n";
				
				//break segment
				s = "";
			}
			else
			{
				//insert character to segment
				s += string_char_at(text,i);
				
				//copy character to new text
				n += string_char_at(text,i);
				
				//check segment width
				if string_width(s) > sprite_width*wordwrap_cutoff && string_char_at(text,i) == " "
				{
					//add newline to new text
					n += "\n";
					
					//break segment
					s = "";
				}
			}
		}
		text = n;
		
		//push other pending messages
		for(var i = 1; i < array_length(pending_texts); i++)
		{
			//push towards 0
			pending_texts[i-1] = pending_texts[i];
		}
		//remove last one
		for(var i = 0, temp = []; i < array_length(pending_texts); i++)
		{
			//check if last one
			if i == array_length(pending_texts)-1
			{
				//apply new array
				pending_texts = temp;
			}
			else
			{
				//copy value
				temp[i] = pending_texts[i];
			}
		}
	}
}

#endregion

#region Input

//check if message typed
if typed_text == text
{
	//check if no text
	if text == ""
	{
		//ready for next line
		nextready = true;
	}
	else if array_length(pending_texts) > 0
	{
		//add continuation text
		if cont_text != continuation_text && !string_count("receiving me?",text)
		{
			cont_text = continuation_text;
		}
		else if string_count("receiving me?",text)
		{
			cont_text = "\n\n(click to respond)";
		}
	}
	
	//check for continuation input
	if place_meeting(x,y,cursor)
	{
		//check for click
		if mouse_check_button_pressed(mb_left) && array_length(pending_texts) > 0
		{
			//fade message
			fade_text = true;
			
			//play sound
			var snd = sn_type_next;
			audio_stop_sound(snd);
			audio_play_sound(snd,1,false);
		}
	}
}
else
{
	#region Skip
	
	//check for continuation input
	if place_meeting(x,y,cursor)
	{
		//check for click
		if mouse_check_button_pressed(mb_right) && array_length(pending_texts) > 0
		{
			//skip
			typed_text = text;
			alarm[0] = -1;
		}
	}
	
	#endregion
}

#endregion

#region Fade

//check if fade
if fade_text
{
	//fade to zero
	text_alpha = lerp(text_alpha,0,fade_step);
	if text_alpha < 1/50 { text_alpha = 0; }
	
	//check if faded
	if text_alpha = 0
	{
		//clear text and disable fade
		typed_text = ""; text = ""; cont_text = "";
		text_alpha = 1;
		fade_text = false;
		
		//ready for next message
		nextready = true;
	}
}

#endregion

#region Transparency

//variables
var transp_treshold = 0.2;
var transp_lerpstep = 1/50;

//check if player moving
if (abs(obj_player.phy_linear_velocity_x) > obj_player.horizontal_mvelocity*transp_treshold) || (abs(obj_player.phy_linear_velocity_y) > obj_player.buoyancy_mvelocity*transp_treshold)
{
	//transparent
	image_alpha = lerp(image_alpha,transparent_alpha,transp_lerpstep);
}
else
{
	//opaque
	image_alpha = lerp(image_alpha,1,transp_lerpstep);
	if image_alpha > 99/100 { image_alpha = 1; }
}

#endregion