/// @desc Draw self & typed text

#region Draw self

//variables
var cx = camera_get_view_x(view_camera[0]), cy = camera_get_view_y(view_camera[0]);
var ch = display_get_gui_height(), cw = display_get_gui_width();
var xpos = 1, ypos = 1, padd = 32;

//final coordinates
var fxpos = (cx+cw*xpos)-(sprite_get_width(spr_log)+padd);
var fypos = (cy+ch*ypos)-(sprite_get_height(spr_log)+padd);

//draw
draw_sprite_ext(sprite_index,0,fxpos,fypos,1,1,0,c_white,image_alpha);

//update position
x = fxpos;
y = fypos;

#endregion

//get drawing properties
draw_set_color($4c1d4f);
draw_set_alpha(text_alpha*image_alpha);
draw_set_font(fnt_log);
draw_set_halign(fa_left);
draw_set_valign(fa_top);

//coordinates
var cpad = 16, tx = x+cpad/2, ty = y+cpad;

//draw typed text
draw_text_ext(tx,ty,typed_text+cont_text,-1,sprite_width*0.99);

//reset drawing properties
draw_set_color(c_white);
draw_set_alpha(1);