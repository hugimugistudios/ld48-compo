/// @desc Positive buoyancy

//prefer upright
if phy_rotation != 0
{
	//turn
	phy_rotation = lerp(phy_rotation,0,upright_tendency);
}

//normalize buoyancy relative to world gravty
var w_grav = 4;
physics_apply_force(phy_com_x,phy_com_y,0,phy_mass*-w_grav);

//apply buoyancy
phy_linear_velocity_y += buoyancy_acceleration*buoyancy*-1;

//keep velocity in certain range
phy_linear_velocity_y = clamp(phy_linear_velocity_y,-buoyancy_mvelocity,buoyancy_mvelocity);

//check if far from followpoint
var maxdist = followpoint.maxdist_def/2;
if distance_to_object(followpoint) > maxdist
{
	//apply force towards followpoint
	var im_dist = phy_mass;
	var im_x = lengthdir_x(im_dist,point_direction(x,y,followpoint.x,followpoint.y));
	var im_y = lengthdir_y(im_dist,point_direction(x,y,followpoint.x,followpoint.y));
	physics_apply_impulse(phy_com_x,phy_com_y,im_x,im_y);
	
	//update follow speed
	follow_speed = im_dist;
}
else
{
	//update follow speed
	if follow_speed != 0
	{
		follow_speed = 0;
	}
}

//check if above room
if y < -sprite_height/2
{
	//package received
	instance_destroy();
}