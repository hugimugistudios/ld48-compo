/// @desc Received

//destroy followpoint
instance_destroy(followpoint);

//check if didn't self destruct
if alarm[1] == -1
{
	//check if correct contents
	with(cr_general)
	{
		//check if not completed
		if !mis_completed
		{
			//check contents
			if other.contents[0] == mission[? "objective"][0] && other.contents[1]+mission[? "received"] >= mission[? "objective"][1]
			{
				//mission completed
				mission_completed();
			}
			else if other.contents[0] == mission[? "objective"][0]
			{
				//controller
				var cont = cr_general;
				
				//correct contents but not enough
				cont.mission[? "received"] += other.contents[1];
				
				//add pending log message
				var lg_msg = "Package received but contains inadequate quantities of the asked substance. Send "+string(mission[? "objective"][1]-mission[? "received"])+" more of the same mineral to finish current mission.";
				log_add(lg_msg);
				
				//add mission log
				log_add(cont.mission[? "mlog"]);
				
				//go to next log entry
				obj_log.fade_text = true;
			}
			else
			{
				//wrong contents
				
				//controller
				var cont = cr_general;
				
				//add pending log message
				var lg_msg = "Package has been received but doesn't contain the requested item, inform yourselves with the mission log for adequate information.";
				log_add(lg_msg);
				
				//add mission log
				log_add(cont.mission[? "mlog"]);
				
				//go to next log entry
				obj_log.fade_text = true;
			}
		}
	}
}