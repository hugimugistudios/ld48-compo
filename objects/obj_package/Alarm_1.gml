/// @desc Self destruct

//self destruct
instance_destroy();

//drop all contents
for(var m = 0; m < contents[1]; m++)
{
	//drop
	with(mineral_create(x,y,contents[0]))
	{
		//add randomization to coordinates
		var ran_r = 10;
		phy_position_x += random_range(-ran_r,ran_r);
		phy_position_y += random_range(-ran_r,ran_r);
	}
}

//add log message
var destr_msg = "!!PACKAGE SELF DESTRUCTED!!##Your package remained stuck for too long and self destructed, dropping its contents.";
log_add(destr_msg);

//add mission log
log_add(cr_general.mission[? "mlog"]);

//go to next log entry
obj_log.fade_text = true;

//keep timer on
alarm[1] = stuck_time;