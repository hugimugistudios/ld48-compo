/// @desc Variables

//no animation
image_speed = 0;

//change sprite depending on size of package
switch(object_index)
{
	case obj_package_medium:
		image_index = 1;
		break;
		
	case obj_package_large:
		image_index = 2;
		break;
		
	default:
		image_index = 0;
		break;
}

//contents
contents = [-1,0];

//create followpoint
followpoint = instance_create_layer(x,y,"Instances",obj_package_followpoint);
followpoint.package = other;

//current follow speed
follow_speed = 0;

//max velocity
vertical_mvelocity = 300;

//buoyancy
buoyancy = 0;
buoyancy_acceleration = 3;
buoyancy_mvelocity = vertical_mvelocity/2;

//upright tendency
upright_tendency = 1/9;

//stuck timer
stuck_time = 1*(60*room_speed);
alarm[0] = stuck_time;

//play sound
var snd = sn_package;
audio_stop_sound(snd);
audio_play_sound(snd,1,false);