/// @desc Stuck

//got stuck
var stuck_msg = "!!PACKAGE STUCK!!##Your package has gotten stuck. To finish sending the package find and make way for the package.";
log_add(stuck_msg);

//add mission log
log_add(cr_general.mission[? "mlog"]);

//go to next log entry
obj_log.fade_text = true;

//start self destruction timer
alarm[1] = stuck_time*3;