/// @desc End loading screen

#region Disable loading screen

//check if loading
if global.loading
{
	//no more loading
	global.loading = false;
}

#endregion

//reset alarm
alarm[0] = -1;