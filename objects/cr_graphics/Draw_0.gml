/// @desc Drawing

#region HUD

//position
var xpos = 1, ypos = 0, padd = 128;

//variables
var cx = camera_get_view_x(view_camera[0]), cy = camera_get_view_y(view_camera[0]);
var ch = camera_get_view_height(view_camera[0]), cw = camera_get_view_width(view_camera[0]);
var h_x = cx+((cw*xpos)-padd/3), h_y = cy+(ch*ypos)+(sprite_get_height(spr_hud_valve)/sprite_get_height(spr_valve_meter))+34;

//valve meter
draw_sprite(spr_valve_meter,2,h_x,h_y);
draw_sprite_ext(spr_valve_meter,1,h_x,h_y,1,(obj_player.buoyancy+1)/2,0,c_white,1);
draw_sprite(spr_valve_meter,0,h_x,h_y);

#region Inventory

//variables
var inv = obj_player.inventory;
var mnrl_spr = -1;
var mrl_name = "";
var text_col = $e8e0df;

//position
var xpos = 0, ypos = 0, padd = 48, t_offsetx = 2, t_offsety = 4;

//check inventory
if inv[0] != -1
{
	//get correct sprite and name
	switch(inv[0])
	{
		case mineral.rock:
			mnrl_spr = spr_mineral_rock;
			mrl_name = "Igneous rock";
			break;
			
		case mineral.bauxite:
			mnrl_spr = spr_mineral_bauxite;
			mrl_name = "Bauxite";
			break;
			
		case mineral.phosphorus:
			mnrl_spr = spr_mineral_phosphorus;
			mrl_name = "Red Phosphorus";
			break;
			
		case mineral.iron:
			mnrl_spr = spr_mineral_iron;
			mrl_name = "Iron Ore";
			break;
			
		case mineral.obsidian:
			mnrl_spr = spr_mineral_obsidian;
			mrl_name = "Obsidian";
			break;
			
		case mineral.gold:
			mnrl_spr = spr_mineral_gold;
			mrl_name = "Gold";
			break;
			
		case mineral.diamond:
			mnrl_spr = spr_mineral_diamond;
			mrl_name = "Diamond";
			break;
			
		case mineral.volcanium:
			mnrl_spr = spr_mineral_volcanium;
			mrl_name = "Volcanium";
			break;
			
		case mineral.aluminium:
			mnrl_spr = spr_mineral_aluminium;
			mrl_name = "Elemental Aluminium";
			break;
			
		case mineral.carbonite:
			mnrl_spr = spr_mineral_carbonite;
			mrl_name = "Carbonite";
			break;
			
		case mineral.daurite:
			mnrl_spr = spr_mineral_daurite;
			mrl_name = "Daurite";
			break;
			
		case mineral.zirconium:
			mnrl_spr = spr_mineral_zirconium;
			mrl_name = "Zirconium";
			break;
			
		case mineral.diamonite:
			mnrl_spr = spr_mineral_diamonite;
			mrl_name = "Crystal Diamonite";
			break;
			
		case mineral.ludium:
			mnrl_spr = spr_mineral_ludium;
			mrl_name = "Ludium";
			break;
			
		case mineral.ludumdaurite48:
			mnrl_spr = spr_mineral_ludumdaurite48;
			mrl_name = "Ludumdaurite-48";
			break;
			
		case mineral.hyperdiamonite:
			mnrl_spr = spr_mineral_hyperdiamonite;
			mrl_name = "Pure Hyperdiamonite";
			break;
		
		default:
			mnrl_spr = spr_mineral;
			mrl_name = "NULL";
			break;
	}
	
	//draw inventory items
	draw_sprite(mnrl_spr,0,(cx+cw*xpos)+padd,(cy+ch*ypos)+padd);
	
	//draw amount
	draw_set_font(fnt_amount);
	draw_set_halign(fa_left);
	draw_set_valign(fa_bottom);
	draw_set_color(text_col);
	if inv[0] == mineral.aluminium || inv[0] == mineral.diamonite || inv[0] == mineral.hyperdiamonite { draw_set_color($b8a44f); }
	draw_text((((cx+cw*xpos)+padd)-sprite_get_width(mnrl_spr)/2)+t_offsetx,(((cy+ch*ypos)+padd)+sprite_get_height(mnrl_spr)/2)+t_offsety,inv[1]);
	draw_set_color(c_white);
	
	//draw name
	draw_set_font(fnt_amount);
	draw_set_halign(fa_left);
	draw_set_valign(fa_middle);
	draw_set_color(text_col);
	draw_text(((cx+cw*xpos)+padd),((cy+ch*ypos)+padd),"  "+string(mrl_name));
	draw_set_color(c_white);
	
	#region Buttons
	
	//distance
	var btn_dist = 256;

	//check if cursor is close enough
	if instance_exists(cursor) && point_distance(cursor.x,cursor.y,((cx+cw*xpos)+padd),((cy+ch*ypos)+padd)) < btn_dist
	{
		//button hitboxes
		var btn_yoffset = 32, btn_pad = 2, btn1_str = "Drop", btn2_str = "Send";
		var btn1_bx = ((cx+cw*xpos)+padd)+padd*0, btn1_by = (cy+ch*ypos)+padd;
		var btn2_bx = ((cx+cw*xpos)+padd)+padd*1.5, btn2_by = ((cy+ch*ypos)+padd);
		var btn1_colx1 = (btn1_bx-btn_pad);
		var btn1_coly1 = ((btn1_by-btn_pad)-(string_height(btn1_str)/3))+btn_yoffset;
		var btn1_colx2 = btn1_bx+string_width(btn1_str)+btn_pad;
		var btn1_coly2 = (btn1_by+(string_height(btn1_str)/3)+btn_pad*2)+btn_yoffset;
		var btn2_colx1 = (btn2_bx-btn_pad);
		var btn2_coly1 = ((btn2_by-btn_pad)-(string_height(btn2_str)/3))+btn_yoffset;
		var btn2_colx2 = btn2_bx+string_width(btn2_str)+btn_pad;
		var btn2_coly2 = (btn2_by+(string_height(btn2_str)/3)+btn_pad*2)+btn_yoffset;
		
		//colors
		var col_sel = $70aeff;
		var col1 = text_col, col2 = text_col;
		
		//check if collding with buttons
		if cursor.x > btn1_colx1 && cursor.x < btn1_colx2 && cursor.y > btn1_coly1 && cursor.y < btn1_coly2
		{
			//drop button
			col1 = col_sel;
		}
		if cursor.x > btn2_colx1 && cursor.x < btn2_colx2 && cursor.y > btn2_coly1 && cursor.y < btn2_coly2
		{
			//send button
			col2 = col_sel;
		}
		
		//draw drop and send buttons
		draw_set_font(fnt_amount);
		draw_set_halign(fa_left);
		draw_set_valign(fa_middle);
		draw_set_color(col1);
		draw_text(((cx+cw*xpos)+padd)+padd*0,((cy+ch*ypos)+padd),"\n\n"+"Drop");
		draw_set_color(col2);
		draw_text(((cx+cw*xpos)+padd)+padd*1.5,((cy+ch*ypos)+padd),"\n\n"+"Send");
		draw_set_color(c_white);
	}
}

#endregion

#endregion

#endregion

#region Loading screen

//check if loading
if global.loading
{
	//set drawing properties
	var bg_col = c_black, bg_alpha = 1;
	draw_set_color(bg_col);
	draw_set_alpha(bg_alpha);
	
	//draw background
	draw_rectangle(cx,cy,cx+cw,cy+ch,false);
	
	//set drawing properties
	var tx_col = c_white, tx_font = fnt_log;
	draw_set_color(tx_col);
	draw_set_font(tx_font);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	
	//draw text
	var tx = "Loading...";
	draw_text(cx+cw*0.5,cy+ch*0.75,tx);
}

#endregion