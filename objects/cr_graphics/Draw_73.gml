/// @desc Draw overlay HUD

//position
var cx = camera_get_view_x(view_camera[0]), cy = camera_get_view_y(view_camera[0]);
var ch = camera_get_view_height(view_camera[0]), cw = camera_get_view_width(view_camera[0]);

#region Game end screen

//check if game ended
if global.End
{
	//set drawing properties
	var bg_col = c_black, bg_alpha = 0.85;
	draw_set_color(bg_col);
	draw_set_alpha(bg_alpha);
	
	//draw background
	draw_rectangle(cx,cy,cx+cw,cy+ch,false);
	
	//set drawing properties
	var tx_col = c_white;
	draw_set_color(tx_col);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	
	//draw end text
	var tx = "The End";
	draw_set_font(fnt_end);
	draw_text(cx+cw/2,cy+ch/2,tx);
	
	//draw credits
	var tx2 = "By Hugimugi (Joonatan Siponen)";
	draw_set_font(fnt_log);
	draw_text(cx+cw/2,cy+ch*0.8,tx2);
}

#endregion

//reset drawing properties
draw_set_color(c_white);
draw_set_alpha(1);