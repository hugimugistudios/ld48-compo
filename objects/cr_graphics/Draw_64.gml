/// @desc Draw debug info

//fps counter
if fps_counter
{
	//string
	var str = "game fps: "+string(fps)+"\n"+"real fps: "+string(fps_real);
	
	//padding
	padd = 32;
	
	//draw set properties
	draw_set_font(fnt_log);
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	
	//draw box
	draw_set_color(c_black);
	draw_rectangle(padd/2,padd/2,string_width(str)*1.25+padd/2,string_height(str)*2+padd/2,false);
	
	//draw text
	draw_set_color(c_white);
	draw_text(padd,padd,str);
}