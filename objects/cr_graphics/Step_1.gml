/// @desc HUD

//variables
var cx = camera_get_view_x(view_camera[0]), cy = camera_get_view_y(view_camera[0]);
var ch = camera_get_view_height(view_camera[0]), cw = camera_get_view_width(view_camera[0]);

#region Bouyancy valve

//position
var xpos = 1, ypos = 0, padd = 128;

//check if it exists
if !instance_exists(obj_hud_valve)
{
	//create
	instance_create_layer((cx+cw*xpos)-padd,(cy+ch*ypos)+padd/2,"HUD",obj_point);
	instance_create_layer((cx+cw*xpos)-padd,(cy+ch*ypos)+padd/2,"HUD",obj_hud_valve);
}
else
{
	//update their position
	obj_hud_valve.phy_position_x = (cx+cw*xpos)-padd;
	obj_hud_valve.phy_position_y = (cy+ch*ypos)+padd/2;
	with(obj_hud_valve)
	{
		instance_nearest(x,y,obj_point).phy_position_x = (cx+cw*xpos)-padd;
		instance_nearest(x,y,obj_point).phy_position_y = (cy+ch*ypos)+padd/2;
	}
}

#endregion

#region Log

//check if it exists
if !instance_exists(obj_log)
{
	//position
	var xpos = 1, ypos = 1, padd = 32;
	
	//create
	instance_create_layer((cx+cw*xpos)-sprite_get_width(spr_log)+padd,(cy+ch*ypos)-sprite_get_height(spr_log)+padd,"HUD",obj_log);
}

#endregion