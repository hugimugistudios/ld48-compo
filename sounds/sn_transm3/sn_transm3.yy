{
  "compression": 0,
  "volume": 0.75,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sn_transm3.wav",
  "duration": 0.664751,
  "parent": {
    "name": "transmutation",
    "path": "folders/Audio/SFX/transmutation.yy",
  },
  "resourceVersion": "1.0",
  "name": "sn_transm3",
  "tags": [],
  "resourceType": "GMSound",
}