{
  "compression": 0,
  "volume": 0.7,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sn_types2.wav",
  "duration": 0.16051,
  "parent": {
    "name": "typing",
    "path": "folders/Audio/SFX/typing.yy",
  },
  "resourceVersion": "1.0",
  "name": "sn_types2",
  "tags": [],
  "resourceType": "GMSound",
}