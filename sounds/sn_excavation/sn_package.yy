{
  "compression": 0,
  "volume": 0.6,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sn_package.wav",
  "duration": 0.240714,
  "parent": {
    "name": "SFX",
    "path": "folders/Audio/SFX.yy",
  },
  "resourceVersion": "1.0",
  "name": "sn_package",
  "tags": [],
  "resourceType": "GMSound",
}