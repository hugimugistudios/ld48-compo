{
  "compression": 0,
  "volume": 0.4,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sn_drop3.wav",
  "duration": 1.317041,
  "parent": {
    "name": "drop",
    "path": "folders/Audio/SFX/drop.yy",
  },
  "resourceVersion": "1.0",
  "name": "sn_drop3",
  "tags": [],
  "resourceType": "GMSound",
}