{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sn_type_next.wav",
  "duration": 1.661032,
  "parent": {
    "name": "typing",
    "path": "folders/Audio/SFX/typing.yy",
  },
  "resourceVersion": "1.0",
  "name": "sn_type_next",
  "tags": [],
  "resourceType": "GMSound",
}