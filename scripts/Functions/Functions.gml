/// Mineral create
function mineral_create(xpos,ypos,mnrl)
{
	//variables
	var mnrl_id = noone;
	
	//create correct mineral
	switch(mnrl)
	{
		case mineral.rock:
			mnrl_id = obj_mineral_rock;
			break;
			
		case mineral.bauxite:
			mnrl_id = obj_mineral_bauxite;
			break;
			
		case mineral.phosphorus:
			mnrl_id = obj_mineral_phosphorus;
			break;
			
		case mineral.iron:
			mnrl_id = obj_mineral_iron;
			break;
			
		case mineral.obsidian:
			mnrl_id = obj_mineral_obsidian;
			break;
			
		case mineral.gold:
			mnrl_id = obj_mineral_gold;
			break;
			
		case mineral.diamond:
			mnrl_id = obj_mineral_diamond;
			break;
			
		case mineral.volcanium:
			mnrl_id = obj_mineral_volcanium;
			break;
			
		case mineral.aluminium:
			mnrl_id = obj_mineral_aluminium;
			break;
			
		case mineral.carbonite:
			mnrl_id = obj_mineral_carbonite;
			break;
			
		case mineral.daurite:
			mnrl_id = obj_mineral_daurite;
			break;
			
		case mineral.zirconium:
			mnrl_id = obj_mineral_zirconium;
			break;
			
		case mineral.diamonite:
			mnrl_id = obj_mineral_diamonite;
			break;
			
		case mineral.ludium:
			mnrl_id = obj_mineral_ludium;
			break;
			
		case mineral.ludumdaurite48:
			mnrl_id = obj_mineral_ludumdaurite48;
			break;
			
		case mineral.hyperdiamonite:
			mnrl_id = obj_mineral_hyperdiamonite;
			break;
		
		default:
			mnrl_id = obj_mineral;
			break;
	}
	
	//create it and return id
	return(instance_create_layer(xpos,ypos,"Instances",mnrl_id));
}

/// Transmutation
function transmutate(mnrl1,mnrl2)
{
	//check if minerals transmutate
	for(var t = 0; t < array_length(variable_struct_get(global.transmutations,mnrl1)); t++)
	{
		//check if it matches mineral 2
		if mnrl2 == variable_struct_get(global.transmutations,mnrl1)[t][0]
		{
			//matches, transmutate
			return(variable_struct_get(global.transmutations,mnrl1)[t][1]);
			break;
		}
	}
	
	//didn't transmutate
	return(-1);
}

/// Add pending log message
function log_add(log_msg)
{
	//add log to pending messages
	obj_log.pending_texts[array_length(obj_log.pending_texts)] = log_msg;
}

#region Mission

/// Start mission
function mission_start(m_index)
{
	//controller
	var cont = cr_general;
	
	//get next mission
	cont.mission[? "index"] = m_index;
	
	//start mission
	cont.mission[? "name"] = variable_struct_get(variable_struct_get(global.missions,cont.mission[? "index"]),"name");
	cont.mission[? "objective"] = variable_struct_get(variable_struct_get(global.missions,cont.mission[? "index"]),"objective");
	cont.mission[? "log"] = variable_struct_get(variable_struct_get(global.missions,cont.mission[? "index"]),"log");
	cont.mission[? "mlog"] = variable_struct_get(variable_struct_get(global.missions,cont.mission[? "index"]),"mlog");
	cont.mission[? "log2"] = variable_struct_get(variable_struct_get(global.missions,cont.mission[? "index"]),"log2");
	cont.mission[? "received"] = false;
	
	//add logs to pending messages
	for(var l = 0; l < array_length(cont.mission[? "log"]); l++)
	{
		//add log
		log_add(cont.mission[? "log"][l]);
	}
	
	//add mission log
	log_add(cont.mission[? "mlog"]);
}

/// Set mission
function mission_set(m_index,m_name,m_objective,m_log,m_mlog,m_log2)
{
	//variables
	var cont = cr_general;
	
	//replace mission values with new ones
	cont.mission[? "index"] = m_index;
	cont.mission[? "name"] = m_name;
	cont.mission[? "objective"] = m_objective;
	cont.mission[? "log"] = m_log;
	cont.mission[? "mlog"] = m_mlog;
	cont.mission[? "log2"] = m_log2;
	cont.mission[? "received"] = 0;
	
	//add logs to pending messages
	for(var l = 0; l < array_length(cont.mission[? "log"]); l++)
	{
		//add log
		log_add(cont.mission[? "log"][l]);
	}
	
	//add mission log
	log_add(cont.mission[? "mlog"]);
}

/// Mission completed
function mission_completed()
{
	//variables
	var cont = cr_general;
	var ms_msg = "[MISSION SUCCESSFUL]##Your package has been received and contains enough of the requested item.";
	
	//mark as completed
	cont.mis_completed = true;
	
	//add mission success log message
	log_add(ms_msg);
	
	//add other mission success logs to pending messages
	for(var l = 0; l < array_length(cont.mission[? "log2"]); l++)
	{
		//add log
		log_add(cont.mission[? "log2"][l]);
	}
	
	//go to next log entry
	obj_log.fade_text = true;
}

#endregion