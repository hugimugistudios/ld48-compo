/// Functions
Functions();

/// Enums
enum mineral
{
	null,
	rock,
	bauxite,
	phosphorus,
	iron,
	obsidian,
	gold,
	diamond,
	volcanium,
	aluminium,
	carbonite,
	daurite,
	zirconium,
	diamonite,
	ludium,
	ludumdaurite48,
	hyperdiamonite
}

#region Global variables

//loading screen
global.loading = true;

//game ended
global.End = false;

//missions
global.missions = {};

//transmutations
global.transmutations = {};

//mp grid
var tilesz = 32;
global.mp_grid = mp_grid_create(0,0,room_width/tilesz,room_height/tilesz,32,32);

#region Fetch from file

#region Missions

//get transmutation json string
var msjson = "mss.ns";
if file_exists(msjson)
{
	//open and read json file contents
	var msjson_file = file_text_open_read(msjson);
	var msansm_json = "";
	for(var l = 0; l < 10000; l++)
	{
		//read line
		msansm_json += file_text_readln(msjson_file);
		
		//check if end of file
		if file_text_eof(msjson_file)
		{
			//file ended
			break;
		}
	}
	file_text_close(msjson_file);
	
	//update missions
	global.missions = json_parse(msansm_json);
}
else
{
	//no file found
	show_error("No \""+string(msjson)+"\" file found at root directory.",true);
}

#endregion

#region Transmutations

//get transmutationS json string
var trjson = "transm.json";
if file_exists(trjson)
{
	//open and read json file contents
	var trjson_file = file_text_open_read(trjson);
	var transm_json = "";
	for(var l = 0; l < 1000; l++)
	{
		//read line
		transm_json += file_text_readln(trjson_file);
		
		//check if end of file
		if file_text_eof(trjson_file)
		{
			//file ended
			break;
		}
	}
	file_text_close(trjson_file);
	
	//get transmutations
	global.transmutations = json_parse(transm_json);
}
else
{
	//no file found
	show_error("No \""+string(trjson)+"\" file found at root directory.",true);
}

#endregion

#endregion

#endregion