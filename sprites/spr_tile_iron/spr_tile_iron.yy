{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"9d2851ff-2d72-4aa8-b9fd-1c40fe98acc8","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9d2851ff-2d72-4aa8-b9fd-1c40fe98acc8","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":{"name":"d34d923f-3937-4f2e-ae54-e229975cd226","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_iron","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"9d2851ff-2d72-4aa8-b9fd-1c40fe98acc8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"12537a85-b624-4a03-82b4-4de1c655d324","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"12537a85-b624-4a03-82b4-4de1c655d324","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":{"name":"d34d923f-3937-4f2e-ae54-e229975cd226","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_iron","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"12537a85-b624-4a03-82b4-4de1c655d324","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"50c297cf-cd0f-4791-a934-677e8b987ddf","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"50c297cf-cd0f-4791-a934-677e8b987ddf","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":{"name":"d34d923f-3937-4f2e-ae54-e229975cd226","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_iron","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"50c297cf-cd0f-4791-a934-677e8b987ddf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3cfdc9f2-b562-486b-81c1-015738d789ca","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3cfdc9f2-b562-486b-81c1-015738d789ca","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":{"name":"d34d923f-3937-4f2e-ae54-e229975cd226","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_iron","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"3cfdc9f2-b562-486b-81c1-015738d789ca","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8237e08a-aa27-4383-94db-ee4dd89be5c9","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8237e08a-aa27-4383-94db-ee4dd89be5c9","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":{"name":"d34d923f-3937-4f2e-ae54-e229975cd226","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_iron","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"8237e08a-aa27-4383-94db-ee4dd89be5c9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f8f56056-ae7f-4e11-b503-9c212b060b68","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f8f56056-ae7f-4e11-b503-9c212b060b68","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":{"name":"d34d923f-3937-4f2e-ae54-e229975cd226","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_iron","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"f8f56056-ae7f-4e11-b503-9c212b060b68","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b88bd57d-78fa-4986-84f7-1cf257ec0463","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b88bd57d-78fa-4986-84f7-1cf257ec0463","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":{"name":"d34d923f-3937-4f2e-ae54-e229975cd226","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_iron","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"b88bd57d-78fa-4986-84f7-1cf257ec0463","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d1cd94c5-1155-4a21-9bbe-96419248c854","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d1cd94c5-1155-4a21-9bbe-96419248c854","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":{"name":"d34d923f-3937-4f2e-ae54-e229975cd226","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_iron","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"d1cd94c5-1155-4a21-9bbe-96419248c854","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2f6aa50f-75cf-4b38-9003-627e1f03c4c6","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2f6aa50f-75cf-4b38-9003-627e1f03c4c6","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":{"name":"d34d923f-3937-4f2e-ae54-e229975cd226","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_iron","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"2f6aa50f-75cf-4b38-9003-627e1f03c4c6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"10cbfcec-da02-4bf6-9414-9aa85a32cf4d","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"10cbfcec-da02-4bf6-9414-9aa85a32cf4d","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":{"name":"d34d923f-3937-4f2e-ae54-e229975cd226","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_iron","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"10cbfcec-da02-4bf6-9414-9aa85a32cf4d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8a103529-81a6-4755-8cd4-cef19504d0e1","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8a103529-81a6-4755-8cd4-cef19504d0e1","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":{"name":"d34d923f-3937-4f2e-ae54-e229975cd226","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_iron","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"8a103529-81a6-4755-8cd4-cef19504d0e1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d347e0da-f214-4a29-a72a-e61e49e44015","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d347e0da-f214-4a29-a72a-e61e49e44015","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":{"name":"d34d923f-3937-4f2e-ae54-e229975cd226","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_iron","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"d347e0da-f214-4a29-a72a-e61e49e44015","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8a5cac05-6103-4e25-a4ed-270c0affd711","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8a5cac05-6103-4e25-a4ed-270c0affd711","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"LayerId":{"name":"d34d923f-3937-4f2e-ae54-e229975cd226","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_iron","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","name":"8a5cac05-6103-4e25-a4ed-270c0affd711","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_tile_iron","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 13.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5ae50422-a0a1-4974-abbe-28f8b6d08513","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9d2851ff-2d72-4aa8-b9fd-1c40fe98acc8","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2046abd2-959d-4373-ad54-b3de7a3b101b","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"12537a85-b624-4a03-82b4-4de1c655d324","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ef218fc8-eb03-48bb-8116-1ef3a8e7872a","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"50c297cf-cd0f-4791-a934-677e8b987ddf","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"630f4fd0-7243-4890-91dd-bb43965f270d","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3cfdc9f2-b562-486b-81c1-015738d789ca","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7efcdb89-6ec3-4959-90d7-d971e5f6e5ca","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8237e08a-aa27-4383-94db-ee4dd89be5c9","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a187b1c8-f6d6-4222-9b31-017ee31c2fd1","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f8f56056-ae7f-4e11-b503-9c212b060b68","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"27da6813-f435-4298-9a04-8144a5564836","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b88bd57d-78fa-4986-84f7-1cf257ec0463","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"95c908cc-db9c-4172-873a-375e26fb9a38","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d1cd94c5-1155-4a21-9bbe-96419248c854","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d7a52bb4-d4fb-4b02-95e0-d39f2342311c","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2f6aa50f-75cf-4b38-9003-627e1f03c4c6","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"076f8120-6ae7-438a-b99c-3af3851f9b99","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"10cbfcec-da02-4bf6-9414-9aa85a32cf4d","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"593ce109-630b-40db-9835-aaa80285ccb9","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8a103529-81a6-4755-8cd4-cef19504d0e1","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"609e1c43-7d3d-484c-97ec-dc22b68afd5c","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d347e0da-f214-4a29-a72a-e61e49e44015","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3a102770-a7ca-4737-841f-34907e22ff28","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8a5cac05-6103-4e25-a4ed-270c0affd711","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_tile_iron","path":"sprites/spr_tile_iron/spr_tile_iron.yy",},
    "resourceVersion": "1.3",
    "name": "spr_tile_iron",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d34d923f-3937-4f2e-ae54-e229975cd226","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": {
    "left": 0,
    "top": 0,
    "right": 0,
    "bottom": 0,
    "guideColour": [
      4294902015,
      4294902015,
      4294902015,
      4294902015,
    ],
    "highlightColour": 1728023040,
    "highlightStyle": 0,
    "enabled": false,
    "tileMode": [
      0,
      0,
      0,
      0,
      0,
    ],
    "resourceVersion": "1.0",
    "loadedVersion": null,
    "resourceType": "GMNineSliceData",
  },
  "parent": {
    "name": "Tiles",
    "path": "folders/Graphics/Tiles.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_tile_iron",
  "tags": [],
  "resourceType": "GMSprite",
}