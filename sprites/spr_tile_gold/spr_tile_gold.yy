{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"df6bbf50-00da-4000-a961-a1fa92bab5cd","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"df6bbf50-00da-4000-a961-a1fa92bab5cd","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":{"name":"1c4d071e-f3f1-41df-bef6-93015b5c7b27","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_gold","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"df6bbf50-00da-4000-a961-a1fa92bab5cd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"089a5879-45f9-4cce-956c-1686c1efef1b","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"089a5879-45f9-4cce-956c-1686c1efef1b","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":{"name":"1c4d071e-f3f1-41df-bef6-93015b5c7b27","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_gold","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"089a5879-45f9-4cce-956c-1686c1efef1b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ca5a4c2d-8499-4b17-9a01-dcfdb583b6a2","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ca5a4c2d-8499-4b17-9a01-dcfdb583b6a2","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":{"name":"1c4d071e-f3f1-41df-bef6-93015b5c7b27","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_gold","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"ca5a4c2d-8499-4b17-9a01-dcfdb583b6a2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d2e2d3b1-ce2c-424a-a03b-02a0a808750c","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d2e2d3b1-ce2c-424a-a03b-02a0a808750c","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":{"name":"1c4d071e-f3f1-41df-bef6-93015b5c7b27","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_gold","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"d2e2d3b1-ce2c-424a-a03b-02a0a808750c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"686c955f-f2df-409b-a907-03bc710977ca","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"686c955f-f2df-409b-a907-03bc710977ca","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":{"name":"1c4d071e-f3f1-41df-bef6-93015b5c7b27","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_gold","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"686c955f-f2df-409b-a907-03bc710977ca","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c109f59f-7854-4899-94ec-eef0854e00c3","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c109f59f-7854-4899-94ec-eef0854e00c3","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":{"name":"1c4d071e-f3f1-41df-bef6-93015b5c7b27","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_gold","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"c109f59f-7854-4899-94ec-eef0854e00c3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d763701f-e43d-4658-95e6-08081221703e","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d763701f-e43d-4658-95e6-08081221703e","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":{"name":"1c4d071e-f3f1-41df-bef6-93015b5c7b27","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_gold","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"d763701f-e43d-4658-95e6-08081221703e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"80af56c3-a680-4b76-aff5-14e0e1efa3b2","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"80af56c3-a680-4b76-aff5-14e0e1efa3b2","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":{"name":"1c4d071e-f3f1-41df-bef6-93015b5c7b27","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_gold","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"80af56c3-a680-4b76-aff5-14e0e1efa3b2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"67d34b2a-6dc7-4a55-b97f-86dc98a49d78","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"67d34b2a-6dc7-4a55-b97f-86dc98a49d78","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":{"name":"1c4d071e-f3f1-41df-bef6-93015b5c7b27","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_gold","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"67d34b2a-6dc7-4a55-b97f-86dc98a49d78","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9b8ee39a-5625-4bc0-9f49-79f3f1206ed0","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9b8ee39a-5625-4bc0-9f49-79f3f1206ed0","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":{"name":"1c4d071e-f3f1-41df-bef6-93015b5c7b27","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_gold","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"9b8ee39a-5625-4bc0-9f49-79f3f1206ed0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"767d1b30-015e-4b84-b053-6bcf85d8cc25","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"767d1b30-015e-4b84-b053-6bcf85d8cc25","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":{"name":"1c4d071e-f3f1-41df-bef6-93015b5c7b27","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_gold","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"767d1b30-015e-4b84-b053-6bcf85d8cc25","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"edb1334a-966b-4109-aa55-66ae7aa7a766","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"edb1334a-966b-4109-aa55-66ae7aa7a766","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":{"name":"1c4d071e-f3f1-41df-bef6-93015b5c7b27","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_gold","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"edb1334a-966b-4109-aa55-66ae7aa7a766","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"59ea8dfb-a7e0-4efd-80dd-052303b075a3","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"59ea8dfb-a7e0-4efd-80dd-052303b075a3","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"LayerId":{"name":"1c4d071e-f3f1-41df-bef6-93015b5c7b27","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_gold","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","name":"59ea8dfb-a7e0-4efd-80dd-052303b075a3","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_tile_gold","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 13.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"b43c4a13-23db-482f-b10d-6070141a7f68","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"df6bbf50-00da-4000-a961-a1fa92bab5cd","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"48c9332e-31a7-44b4-a0de-511f024c1d16","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"089a5879-45f9-4cce-956c-1686c1efef1b","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a0baf15b-973d-4895-9d59-353f7d6d844c","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ca5a4c2d-8499-4b17-9a01-dcfdb583b6a2","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f1830792-4e5c-429d-939b-612c3a5859bc","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d2e2d3b1-ce2c-424a-a03b-02a0a808750c","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fb8f1f3d-7dd9-4ad9-9bec-08c883eab03f","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"686c955f-f2df-409b-a907-03bc710977ca","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e07316ea-153b-49ee-a8db-7174d56b779c","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c109f59f-7854-4899-94ec-eef0854e00c3","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"35e3bb0e-fa06-4bf8-81fc-0d627b29946d","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d763701f-e43d-4658-95e6-08081221703e","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5957173a-47a5-4f55-83a6-ad7880f2e708","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"80af56c3-a680-4b76-aff5-14e0e1efa3b2","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a62bd5dd-7e1d-4e20-aa91-6a08d028de33","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"67d34b2a-6dc7-4a55-b97f-86dc98a49d78","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"563f3957-b29b-4114-8646-73ef1b43f6c4","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9b8ee39a-5625-4bc0-9f49-79f3f1206ed0","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"584bfd44-fed3-4a46-8d17-e0c7e62fe8f8","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"767d1b30-015e-4b84-b053-6bcf85d8cc25","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7dcdfe8a-d2f7-4874-9580-cbf6225e499f","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"edb1334a-966b-4109-aa55-66ae7aa7a766","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"64f96c22-8ce1-4872-a572-b2811339593b","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"59ea8dfb-a7e0-4efd-80dd-052303b075a3","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_tile_gold","path":"sprites/spr_tile_gold/spr_tile_gold.yy",},
    "resourceVersion": "1.3",
    "name": "spr_tile_gold",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"1c4d071e-f3f1-41df-bef6-93015b5c7b27","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": {
    "left": 0,
    "top": 0,
    "right": 0,
    "bottom": 0,
    "guideColour": [
      4294902015,
      4294902015,
      4294902015,
      4294902015,
    ],
    "highlightColour": 1728023040,
    "highlightStyle": 0,
    "enabled": false,
    "tileMode": [
      0,
      0,
      0,
      0,
      0,
    ],
    "resourceVersion": "1.0",
    "loadedVersion": null,
    "resourceType": "GMNineSliceData",
  },
  "parent": {
    "name": "Tiles",
    "path": "folders/Graphics/Tiles.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_tile_gold",
  "tags": [],
  "resourceType": "GMSprite",
}