{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e74c7190-a202-4f51-b342-6beb9555d6c9","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e74c7190-a202-4f51-b342-6beb9555d6c9","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":{"name":"31859d3d-9202-4e43-b8b6-85eb09acb470","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_obsidian","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"e74c7190-a202-4f51-b342-6beb9555d6c9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1d57bcf7-5874-4072-a765-2d5eb52798dd","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1d57bcf7-5874-4072-a765-2d5eb52798dd","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":{"name":"31859d3d-9202-4e43-b8b6-85eb09acb470","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_obsidian","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"1d57bcf7-5874-4072-a765-2d5eb52798dd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8834dd44-5e4f-458d-87f5-58d9b56af37a","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8834dd44-5e4f-458d-87f5-58d9b56af37a","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":{"name":"31859d3d-9202-4e43-b8b6-85eb09acb470","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_obsidian","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"8834dd44-5e4f-458d-87f5-58d9b56af37a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2cbc075b-04a8-4aca-a98e-de2a7d398593","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2cbc075b-04a8-4aca-a98e-de2a7d398593","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":{"name":"31859d3d-9202-4e43-b8b6-85eb09acb470","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_obsidian","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"2cbc075b-04a8-4aca-a98e-de2a7d398593","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e405e219-91d4-4d0a-b75a-ba018725b249","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e405e219-91d4-4d0a-b75a-ba018725b249","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":{"name":"31859d3d-9202-4e43-b8b6-85eb09acb470","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_obsidian","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"e405e219-91d4-4d0a-b75a-ba018725b249","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a4a591ed-1d58-48a2-a3c2-49171339223f","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a4a591ed-1d58-48a2-a3c2-49171339223f","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":{"name":"31859d3d-9202-4e43-b8b6-85eb09acb470","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_obsidian","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"a4a591ed-1d58-48a2-a3c2-49171339223f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"74a63ed2-c73e-45e4-977f-0c31456191d1","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"74a63ed2-c73e-45e4-977f-0c31456191d1","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":{"name":"31859d3d-9202-4e43-b8b6-85eb09acb470","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_obsidian","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"74a63ed2-c73e-45e4-977f-0c31456191d1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2abc66ab-1a42-447b-be70-23a399242e8b","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2abc66ab-1a42-447b-be70-23a399242e8b","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":{"name":"31859d3d-9202-4e43-b8b6-85eb09acb470","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_obsidian","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"2abc66ab-1a42-447b-be70-23a399242e8b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5172622d-e105-4657-b4ac-c3ed3ecbd5d1","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5172622d-e105-4657-b4ac-c3ed3ecbd5d1","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":{"name":"31859d3d-9202-4e43-b8b6-85eb09acb470","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_obsidian","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"5172622d-e105-4657-b4ac-c3ed3ecbd5d1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6fedd152-44c1-41a4-b2de-e68cb409a673","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6fedd152-44c1-41a4-b2de-e68cb409a673","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":{"name":"31859d3d-9202-4e43-b8b6-85eb09acb470","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_obsidian","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"6fedd152-44c1-41a4-b2de-e68cb409a673","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"19ff6f05-8f66-49d7-9576-481510f8d7e1","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"19ff6f05-8f66-49d7-9576-481510f8d7e1","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":{"name":"31859d3d-9202-4e43-b8b6-85eb09acb470","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_obsidian","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"19ff6f05-8f66-49d7-9576-481510f8d7e1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"45d1f651-fae9-4988-9015-49578224d8a4","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"45d1f651-fae9-4988-9015-49578224d8a4","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":{"name":"31859d3d-9202-4e43-b8b6-85eb09acb470","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_obsidian","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"45d1f651-fae9-4988-9015-49578224d8a4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a18e5a87-cace-4ff0-903e-cc20522baa52","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a18e5a87-cace-4ff0-903e-cc20522baa52","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"LayerId":{"name":"31859d3d-9202-4e43-b8b6-85eb09acb470","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_obsidian","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","name":"a18e5a87-cace-4ff0-903e-cc20522baa52","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_tile_obsidian","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 13.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"8c8438ed-e2a8-4f9e-bb30-5033dbc0629d","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e74c7190-a202-4f51-b342-6beb9555d6c9","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"156586fb-ebe6-4aa4-80b0-de689aec8a3f","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1d57bcf7-5874-4072-a765-2d5eb52798dd","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0cecae00-8e6a-4caa-8b53-a987da6af1b7","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8834dd44-5e4f-458d-87f5-58d9b56af37a","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1c0f3e36-65ce-466e-b532-bf729fbbd6dd","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2cbc075b-04a8-4aca-a98e-de2a7d398593","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"429f1901-4bf5-4567-bd34-b6dee0f1a42e","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e405e219-91d4-4d0a-b75a-ba018725b249","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ef8097b2-4b3b-41a0-ae84-db36af0b94c8","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a4a591ed-1d58-48a2-a3c2-49171339223f","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"08bf003c-1b4f-4aeb-b59d-e7fdb923c0b5","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"74a63ed2-c73e-45e4-977f-0c31456191d1","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ca0b0632-ca20-46ac-bf40-b430191363f0","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2abc66ab-1a42-447b-be70-23a399242e8b","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"78d34973-9585-4b48-9112-aec38eba15a0","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5172622d-e105-4657-b4ac-c3ed3ecbd5d1","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a35116dd-23f6-4de4-ae31-7f2fd35abf83","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6fedd152-44c1-41a4-b2de-e68cb409a673","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a62b553f-4e78-4089-9cfa-f42481a1c71b","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"19ff6f05-8f66-49d7-9576-481510f8d7e1","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6e2d7531-7b9a-4ac1-bca9-02634bfa23bd","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"45d1f651-fae9-4988-9015-49578224d8a4","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"70c2c6a8-8bbc-4aa6-9cca-6747a47b3b46","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a18e5a87-cace-4ff0-903e-cc20522baa52","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_tile_obsidian","path":"sprites/spr_tile_obsidian/spr_tile_obsidian.yy",},
    "resourceVersion": "1.3",
    "name": "spr_tile_obsidian",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"31859d3d-9202-4e43-b8b6-85eb09acb470","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": {
    "left": 0,
    "top": 0,
    "right": 0,
    "bottom": 0,
    "guideColour": [
      4294902015,
      4294902015,
      4294902015,
      4294902015,
    ],
    "highlightColour": 1728023040,
    "highlightStyle": 0,
    "enabled": false,
    "tileMode": [
      0,
      0,
      0,
      0,
      0,
    ],
    "resourceVersion": "1.0",
    "loadedVersion": null,
    "resourceType": "GMNineSliceData",
  },
  "parent": {
    "name": "Tiles",
    "path": "folders/Graphics/Tiles.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_tile_obsidian",
  "tags": [],
  "resourceType": "GMSprite",
}