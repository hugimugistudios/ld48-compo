{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 29,
  "bbox_top": 2,
  "bbox_bottom": 29,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"3dd2d6a9-b97f-4ea1-ba4d-8a36de6b2e0a","path":"sprites/spr_mineral_zirconium/spr_mineral_zirconium.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3dd2d6a9-b97f-4ea1-ba4d-8a36de6b2e0a","path":"sprites/spr_mineral_zirconium/spr_mineral_zirconium.yy",},"LayerId":{"name":"de959911-7b63-4eae-8626-fb031f14c227","path":"sprites/spr_mineral_zirconium/spr_mineral_zirconium.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mineral_zirconium","path":"sprites/spr_mineral_zirconium/spr_mineral_zirconium.yy",},"resourceVersion":"1.0","name":"3dd2d6a9-b97f-4ea1-ba4d-8a36de6b2e0a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"780ae868-9961-4e29-9a6f-641d89687a3e","path":"sprites/spr_mineral_zirconium/spr_mineral_zirconium.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"780ae868-9961-4e29-9a6f-641d89687a3e","path":"sprites/spr_mineral_zirconium/spr_mineral_zirconium.yy",},"LayerId":{"name":"de959911-7b63-4eae-8626-fb031f14c227","path":"sprites/spr_mineral_zirconium/spr_mineral_zirconium.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mineral_zirconium","path":"sprites/spr_mineral_zirconium/spr_mineral_zirconium.yy",},"resourceVersion":"1.0","name":"780ae868-9961-4e29-9a6f-641d89687a3e","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_mineral_zirconium","path":"sprites/spr_mineral_zirconium/spr_mineral_zirconium.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"d63bc43c-4958-4a16-af2b-8685f41b3d64","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3dd2d6a9-b97f-4ea1-ba4d-8a36de6b2e0a","path":"sprites/spr_mineral_zirconium/spr_mineral_zirconium.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0433fdb6-c378-478e-916f-678210550e38","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"780ae868-9961-4e29-9a6f-641d89687a3e","path":"sprites/spr_mineral_zirconium/spr_mineral_zirconium.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_mineral_zirconium","path":"sprites/spr_mineral_zirconium/spr_mineral_zirconium.yy",},
    "resourceVersion": "1.3",
    "name": "spr_mineral_zirconium",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"de959911-7b63-4eae-8626-fb031f14c227","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Minerals",
    "path": "folders/Graphics/Minerals.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_mineral_zirconium",
  "tags": [],
  "resourceType": "GMSprite",
}