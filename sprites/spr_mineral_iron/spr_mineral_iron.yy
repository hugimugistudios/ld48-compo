{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 29,
  "bbox_top": 2,
  "bbox_bottom": 29,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"2511b4aa-0f53-453b-83a9-67da99407f6c","path":"sprites/spr_mineral_iron/spr_mineral_iron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2511b4aa-0f53-453b-83a9-67da99407f6c","path":"sprites/spr_mineral_iron/spr_mineral_iron.yy",},"LayerId":{"name":"e43b33df-afce-469e-a662-d64fbd2e2b4d","path":"sprites/spr_mineral_iron/spr_mineral_iron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mineral_iron","path":"sprites/spr_mineral_iron/spr_mineral_iron.yy",},"resourceVersion":"1.0","name":"2511b4aa-0f53-453b-83a9-67da99407f6c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"65165607-ee2e-4625-99fa-34387ea6297c","path":"sprites/spr_mineral_iron/spr_mineral_iron.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"65165607-ee2e-4625-99fa-34387ea6297c","path":"sprites/spr_mineral_iron/spr_mineral_iron.yy",},"LayerId":{"name":"e43b33df-afce-469e-a662-d64fbd2e2b4d","path":"sprites/spr_mineral_iron/spr_mineral_iron.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mineral_iron","path":"sprites/spr_mineral_iron/spr_mineral_iron.yy",},"resourceVersion":"1.0","name":"65165607-ee2e-4625-99fa-34387ea6297c","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_mineral_iron","path":"sprites/spr_mineral_iron/spr_mineral_iron.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a9be70af-f220-49fe-9f69-5d4750d29794","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2511b4aa-0f53-453b-83a9-67da99407f6c","path":"sprites/spr_mineral_iron/spr_mineral_iron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c54579a5-6350-4533-b4b2-8eca20cd82d9","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"65165607-ee2e-4625-99fa-34387ea6297c","path":"sprites/spr_mineral_iron/spr_mineral_iron.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_mineral_iron","path":"sprites/spr_mineral_iron/spr_mineral_iron.yy",},
    "resourceVersion": "1.3",
    "name": "spr_mineral_iron",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"e43b33df-afce-469e-a662-d64fbd2e2b4d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Minerals",
    "path": "folders/Graphics/Minerals.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_mineral_iron",
  "tags": [],
  "resourceType": "GMSprite",
}