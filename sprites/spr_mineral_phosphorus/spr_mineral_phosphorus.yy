{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 29,
  "bbox_top": 2,
  "bbox_bottom": 29,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"1b91dc9f-82e5-45e0-8063-e03af94f1748","path":"sprites/spr_mineral_phosphorus/spr_mineral_phosphorus.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1b91dc9f-82e5-45e0-8063-e03af94f1748","path":"sprites/spr_mineral_phosphorus/spr_mineral_phosphorus.yy",},"LayerId":{"name":"e1a2b2b3-15c7-4841-92c4-6e872ec32e76","path":"sprites/spr_mineral_phosphorus/spr_mineral_phosphorus.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mineral_phosphorus","path":"sprites/spr_mineral_phosphorus/spr_mineral_phosphorus.yy",},"resourceVersion":"1.0","name":"1b91dc9f-82e5-45e0-8063-e03af94f1748","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8559747f-3db0-4167-b3ae-4600c2d0090a","path":"sprites/spr_mineral_phosphorus/spr_mineral_phosphorus.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8559747f-3db0-4167-b3ae-4600c2d0090a","path":"sprites/spr_mineral_phosphorus/spr_mineral_phosphorus.yy",},"LayerId":{"name":"e1a2b2b3-15c7-4841-92c4-6e872ec32e76","path":"sprites/spr_mineral_phosphorus/spr_mineral_phosphorus.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mineral_phosphorus","path":"sprites/spr_mineral_phosphorus/spr_mineral_phosphorus.yy",},"resourceVersion":"1.0","name":"8559747f-3db0-4167-b3ae-4600c2d0090a","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_mineral_phosphorus","path":"sprites/spr_mineral_phosphorus/spr_mineral_phosphorus.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"e7197cf7-5c66-4b07-9c96-eb9cb649f767","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1b91dc9f-82e5-45e0-8063-e03af94f1748","path":"sprites/spr_mineral_phosphorus/spr_mineral_phosphorus.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ba34328b-d5cb-42cb-8ddc-06024f0e15a4","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8559747f-3db0-4167-b3ae-4600c2d0090a","path":"sprites/spr_mineral_phosphorus/spr_mineral_phosphorus.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_mineral_phosphorus","path":"sprites/spr_mineral_phosphorus/spr_mineral_phosphorus.yy",},
    "resourceVersion": "1.3",
    "name": "spr_mineral_phosphorus",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"e1a2b2b3-15c7-4841-92c4-6e872ec32e76","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Minerals",
    "path": "folders/Graphics/Minerals.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_mineral_phosphorus",
  "tags": [],
  "resourceType": "GMSprite",
}