{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"f327a141-ba2f-4e61-8056-20def33d513f","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f327a141-ba2f-4e61-8056-20def33d513f","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":{"name":"598cc4d7-a8fa-42f9-96c7-579038ecf89d","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_phosphorus","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"f327a141-ba2f-4e61-8056-20def33d513f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d407083d-6702-4ca8-94c5-e004b6ab7d05","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d407083d-6702-4ca8-94c5-e004b6ab7d05","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":{"name":"598cc4d7-a8fa-42f9-96c7-579038ecf89d","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_phosphorus","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"d407083d-6702-4ca8-94c5-e004b6ab7d05","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dbbab564-2e40-499b-8720-20d01c39b78f","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dbbab564-2e40-499b-8720-20d01c39b78f","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":{"name":"598cc4d7-a8fa-42f9-96c7-579038ecf89d","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_phosphorus","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"dbbab564-2e40-499b-8720-20d01c39b78f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b5f99516-f97e-49de-af2c-8352f0d809ba","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b5f99516-f97e-49de-af2c-8352f0d809ba","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":{"name":"598cc4d7-a8fa-42f9-96c7-579038ecf89d","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_phosphorus","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"b5f99516-f97e-49de-af2c-8352f0d809ba","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b51a519a-4829-4fa3-9639-8b295f92eace","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b51a519a-4829-4fa3-9639-8b295f92eace","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":{"name":"598cc4d7-a8fa-42f9-96c7-579038ecf89d","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_phosphorus","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"b51a519a-4829-4fa3-9639-8b295f92eace","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c8de8ae4-88bc-4f2a-9121-53e075ebadbf","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c8de8ae4-88bc-4f2a-9121-53e075ebadbf","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":{"name":"598cc4d7-a8fa-42f9-96c7-579038ecf89d","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_phosphorus","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"c8de8ae4-88bc-4f2a-9121-53e075ebadbf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bc0c1097-b12a-40df-959d-381912f3a124","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bc0c1097-b12a-40df-959d-381912f3a124","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":{"name":"598cc4d7-a8fa-42f9-96c7-579038ecf89d","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_phosphorus","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"bc0c1097-b12a-40df-959d-381912f3a124","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"18887f49-235f-49ec-b2c4-d4ffdf630539","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"18887f49-235f-49ec-b2c4-d4ffdf630539","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":{"name":"598cc4d7-a8fa-42f9-96c7-579038ecf89d","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_phosphorus","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"18887f49-235f-49ec-b2c4-d4ffdf630539","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8d4ba2b7-9c90-4b60-8037-557df050ac8e","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8d4ba2b7-9c90-4b60-8037-557df050ac8e","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":{"name":"598cc4d7-a8fa-42f9-96c7-579038ecf89d","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_phosphorus","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"8d4ba2b7-9c90-4b60-8037-557df050ac8e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5bb30c7a-de87-4fb6-8019-962b21872b31","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5bb30c7a-de87-4fb6-8019-962b21872b31","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":{"name":"598cc4d7-a8fa-42f9-96c7-579038ecf89d","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_phosphorus","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"5bb30c7a-de87-4fb6-8019-962b21872b31","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e5ce1f79-ab7c-40f9-96ce-3681a7ef8d75","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e5ce1f79-ab7c-40f9-96ce-3681a7ef8d75","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":{"name":"598cc4d7-a8fa-42f9-96c7-579038ecf89d","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_phosphorus","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"e5ce1f79-ab7c-40f9-96ce-3681a7ef8d75","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f054366a-c273-46a6-bae6-73d79e11cd5c","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f054366a-c273-46a6-bae6-73d79e11cd5c","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":{"name":"598cc4d7-a8fa-42f9-96c7-579038ecf89d","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_phosphorus","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"f054366a-c273-46a6-bae6-73d79e11cd5c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0d4c3201-a4b0-49e9-aaa9-6861494c91f3","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0d4c3201-a4b0-49e9-aaa9-6861494c91f3","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"LayerId":{"name":"598cc4d7-a8fa-42f9-96c7-579038ecf89d","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tile_phosphorus","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","name":"0d4c3201-a4b0-49e9-aaa9-6861494c91f3","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_tile_phosphorus","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 13.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"0e7ffe75-39ff-475e-a761-81c0c985cd4f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f327a141-ba2f-4e61-8056-20def33d513f","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7d6470b3-3918-4ec2-a2c0-744fc61d5b3d","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d407083d-6702-4ca8-94c5-e004b6ab7d05","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bfc00ef2-9720-4b50-97e6-9ebde45c64a3","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dbbab564-2e40-499b-8720-20d01c39b78f","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bcf617bf-412b-4a4f-bb50-4c253e44f74e","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b5f99516-f97e-49de-af2c-8352f0d809ba","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"eadaea6d-6e22-447e-832f-291e7a3e5527","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b51a519a-4829-4fa3-9639-8b295f92eace","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"baba8ceb-c0a0-4b57-95c1-3bbf223541f7","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c8de8ae4-88bc-4f2a-9121-53e075ebadbf","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2c922514-1492-4d9b-95d9-452129cf57d9","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bc0c1097-b12a-40df-959d-381912f3a124","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4f203376-d661-4da7-9410-eae1de6b81c1","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"18887f49-235f-49ec-b2c4-d4ffdf630539","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1e07e867-cb22-408e-aaa2-d6d5b1b29ec6","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8d4ba2b7-9c90-4b60-8037-557df050ac8e","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d08cf35a-ced0-46e8-803a-96f80f1bd415","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5bb30c7a-de87-4fb6-8019-962b21872b31","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6dec0dc9-c58f-4686-b0bd-59aae9d0f724","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e5ce1f79-ab7c-40f9-96ce-3681a7ef8d75","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"878eefbf-6816-478b-9638-2767fce45481","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f054366a-c273-46a6-bae6-73d79e11cd5c","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"812cb092-821c-4592-958a-71ca6f3c2cda","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0d4c3201-a4b0-49e9-aaa9-6861494c91f3","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_tile_phosphorus","path":"sprites/spr_tile_phosphorus/spr_tile_phosphorus.yy",},
    "resourceVersion": "1.3",
    "name": "spr_tile_phosphorus",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"598cc4d7-a8fa-42f9-96c7-579038ecf89d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": {
    "left": 0,
    "top": 0,
    "right": 0,
    "bottom": 0,
    "guideColour": [
      4294902015,
      4294902015,
      4294902015,
      4294902015,
    ],
    "highlightColour": 1728023040,
    "highlightStyle": 0,
    "enabled": false,
    "tileMode": [
      0,
      0,
      0,
      0,
      0,
    ],
    "resourceVersion": "1.0",
    "loadedVersion": null,
    "resourceType": "GMNineSliceData",
  },
  "parent": {
    "name": "Tiles",
    "path": "folders/Graphics/Tiles.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_tile_phosphorus",
  "tags": [],
  "resourceType": "GMSprite",
}