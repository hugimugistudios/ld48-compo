{
  "name": "tl_temptiles",
  "spriteId": {
    "name": "spr_temptiles",
    "path": "sprites/spr_temptiles/spr_temptiles.yy",
  },
  "tileWidth": 32,
  "tileHeight": 32,
  "tilexoff": 0,
  "tileyoff": 0,
  "tilehsep": 0,
  "tilevsep": 0,
  "spriteNoExport": true,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "out_tilehborder": 2,
  "out_tilevborder": 2,
  "out_columns": 4,
  "tile_count": 18,
  "autoTileSets": [],
  "tileAnimationFrames": [],
  "tileAnimationSpeed": 15.0,
  "tileAnimation": {
    "FrameData": [
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
    ],
    "SerialiseFrameCount": 1,
  },
  "macroPageTiles": {
    "SerialiseWidth": 0,
    "SerialiseHeight": 0,
    "TileSerialiseData": [],
  },
  "parent": {
    "name": "Graphics",
    "path": "folders/Graphics.yy",
  },
  "resourceVersion": "1.0",
  "tags": [],
  "resourceType": "GMTileSet",
}